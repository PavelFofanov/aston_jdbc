package com.PavelFofanov;

import com.PavelFofanov.models.Student;
import com.PavelFofanov.repositories.StudentRepository;
import com.PavelFofanov.repositories.StudentRepositoryJDBCImpl;
import com.zaxxer.hikari.HikariDataSource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.List;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        Properties dbProperties = new Properties();
        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/db.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));

        StudentRepository studentRepository = new StudentRepositoryJDBCImpl(dataSource);

        Student student = Student.builder()
                .email("pf@mail.ru")
                .firstName("Pavel")
                .lastName("Fofanov")
                .password("Qwerty001")
                .age(33)
                .build();

        Student student2 = Student.builder()
                .email("sp@gmail.com")
                .firstName("Semen")
                .lastName("Petrov")
                .password("Qwerty002")
                .age(50)
                .build();

        Student student3 = Student.builder()
                .email("rz@yahoo.com")
                .firstName("Roman")
                .lastName("Zenkov")
                .password("Qwerty003")
                .age(18)
                .build();

        studentRepository.save(student3);
        
        List<Student>students = studentRepository.findAll();
        for(Student student1 : students){
            System.out.println(student1);
        }
        studentRepository.delete(6L);

        studentRepository.update(7L, student);
    }
}