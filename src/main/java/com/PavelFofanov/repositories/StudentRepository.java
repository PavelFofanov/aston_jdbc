package com.PavelFofanov.repositories;

import com.PavelFofanov.models.Student;

import java.util.List;

public interface StudentRepository {
    List<Student> findAll();

    void save(Student student);

    void delete(Long id);

    List<Student> update (Long id, Student student);

}
