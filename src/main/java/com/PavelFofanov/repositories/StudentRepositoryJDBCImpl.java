package com.PavelFofanov.repositories;

import com.PavelFofanov.models.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class StudentRepositoryJDBCImpl implements StudentRepository {

    private DataSource dataSource;

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from student order by id";

    //language=SQL
    private static final String SQL_INSERT = "insert into student (email, password, first_name, last_name, age) values (?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_DELETE = "delete from student where (id = ?) ";

    //language=SQL
    private static final String SQL_UPDATE = "update student set first_name = ?, last_name = ?, age = ? where id = ?";

    public StudentRepositoryJDBCImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final Function<ResultSet, Student> studentRowMapper = row -> {
        try {
            return Student.builder()
                    .id(row.getLong("id"))
                    .email(row.getString("email"))
                    .password(row.getString("password"))
                    .firstName(row.getString("first_name"))
                    .lastName(row.getString("last_name"))
                    .age(row.getInt("age"))
                    .build();

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    };

    @Override
    public List<Student> findAll() {
        List<Student> students = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    Student student = studentRowMapper.apply(resultSet);
                    students.add(student);
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return students;
    }

    @Override
    public void save(Student student) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT)) {

            preparedStatement.setString(1, student.getEmail());
            preparedStatement.setString(2, student.getPassword());
            preparedStatement.setString(3, student.getFirstName());
            preparedStatement.setString(4, student.getLastName());
            preparedStatement.setInt(5, student.getAge());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert student");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void delete(Long id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)) {
            preparedStatement.setLong(1, id);
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't delete student");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<Student> update(Long id, Student student) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)) {

            preparedStatement.setString(1, student.getFirstName());
            preparedStatement.setString(2, student.getLastName());
            preparedStatement.setInt(3, student.getAge());
            preparedStatement.setLong(4, id);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't delete student");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return findAll();
    }

}
